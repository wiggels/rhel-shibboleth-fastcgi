#!/bin/bash
# Download latest version of Shibboleth
dnf download --source shibboleth

# Install the SRPM's dependencies
dnf builddep -y shibboleth*.src.rpm

# Rebuild with FastCGI support
rpmbuild --rebuild shibboleth*.src.rpm --with fastcgi