#!/bin/bash
# Install Shibboleth's recommended Service Provider repo as per
# https://shibboleth.net/downloads/service-provider/RPMS/
cp rhel8/shibboleth.repo /etc/yum.repos.d/shibboleth.repo

# Accept Shibboleth's GPG keys
rpm --import https://shibboleth.net/downloads/service-provider/RPMS/repomd.xml.key
rpm --import https://shibboleth.net/downloads/service-provider/RPMS/cantor.repomd.xml.key

# Install EPEL for fcgi-devel
dnf install -y epel-release

# Install required packages for building
dnf install -y \
  make \
  rpm-build \
  rpmdevtools \
  'dnf-command(config-manager)' \
  'dnf-command(download)' \
  'dnf-command(builddep)' \
  fcgi-devel

# For build dependencies: doxygen
dnf config-manager --enable powertools