#!/bin/bash
# Move files to artifact staging
mv ~/rpmbuild/RPMS/* public/rhel/8/
dnf --downloaddir /tmp download liblog4shib2 libsaml12 libxerces-c-3_2 libxml-security-c20 libxmltooling10 opensaml-schemas xmltooling-schemas
mv /tmp/*.rpm public/rhel/8/x86_64/